import React from 'react';
import {Provider} from 'react-redux';
import { configureStore } from './src/store/configureStore';
import { AppContainer } from './src/navigation/navigation';

const store = configureStore();

export default function App() {

  return (
     <Provider store={store}>
        <AppContainer />
     </Provider>
  );
}




