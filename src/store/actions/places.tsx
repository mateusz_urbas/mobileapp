import * as actionType from "./actionTypes";

export interface AddPlace {
    type: actionType.ADD_PLACE,
    placeName: string
}

export interface DeletePlace {
    type: actionType.DELETE_PLACE,
    placeKey: string
}


export const addPlace = (placeName): AddPlace => {
    return {
        type: actionType.ADD_PLACE,
        placeName: placeName
    };
}

export const deletePlace = (key): DeletePlace => {
    return {
        type: actionType.DELETE_PLACE,
        placeKey: key
    }
}