import { AddPlace, DeletePlace } from "./places";

export { addPlace, deletePlace } from "./places"
export type PlacesAction = AddPlace | DeletePlace;
