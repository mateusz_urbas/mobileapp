export const ADD_PLACE = 'ADD_PLACE';
export type ADD_PLACE = typeof ADD_PLACE;

export const DELETE_PLACE = 'DELETE_PLACE';
export type DELETE_PLACE = typeof DELETE_PLACE;

