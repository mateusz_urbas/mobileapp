import { ADD_PLACE, DELETE_PLACE } from "../actions/actionTypes";
import { PlacesAction } from "../actions";

const initState = {
    places: [{
        key: Math.random().toString(),
        name: 'test',
        image: {
            uri: "http://starchop.altervista.org/wp-content/uploads/2015/03/Beauty-Place-Photography-HD-Wallpaper.jpg"
        } 
    }],
};

export const placesReducer = (state = initState, action: PlacesAction) => {
    switch (action.type) {
        case ADD_PLACE:
            return {
                ...state,
                places: state.places.concat({
                    key: Math.random().toString(),
                    name: action.placeName,
                    image: {
                        uri: "http://starchop.altervista.org/wp-content/uploads/2015/03/Beauty-Place-Photography-HD-Wallpaper.jpg"
                    }
                })
            };

        case DELETE_PLACE:
            return {
                ...state,
                places: state.places.filter( place => {
                    return place.key !== action.placeKey;
                }),
            };
        default:
            return state;
    }
};