import React, { Component } from 'react';
import { TouchableOpacity, TouchableNativeFeedback, Text, View, StyleSheet, Platform } from 'react-native';

export interface Props {
    children;
    style?;
    color?;
    onPress();
}



const ButtonWithBackground = (props: Props) => {
        
    const getCostumColor = () => {
        if(props.color !== undefined) {
            return {
                backgroundColor: props.color
            }
        }
    }

    const content = (
        <View style={[styles.button, getCostumColor()]}>
            <Text>{props.children}</Text>
        </View>
    );

    if (Platform.OS === 'android') {
        return (
            <TouchableNativeFeedback onPress={props.onPress}>
                {content}
            </TouchableNativeFeedback>
        );
    }

    return (
        <TouchableOpacity onPress={props.onPress}>
            {content}
        </TouchableOpacity>
    )

    

};


const styles = StyleSheet.create({
    button: {
        padding: 10,
        margin: 5,
        backgroundColor: '#29aaf4',
        borderRadius: 5,
        borderWidth: 1,
        borderColor: 'black',
    }

});

export default ButtonWithBackground;