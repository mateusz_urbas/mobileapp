import React from 'react';
import { Text, StyleSheet } from 'react-native';

export interface Props {
    children
    style?;
}

const HeadingText = (props:Props) => (
    <Text {...props} style={[styles.textHeading, props.style]}>
        {props.children}
    </Text>

);

const styles = StyleSheet.create({
    textHeading: {
        fontSize: 28,
        fontWeight: 'bold',
    },
});

export default HeadingText;