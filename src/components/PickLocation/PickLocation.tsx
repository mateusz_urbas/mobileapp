import React, { Component } from 'react';
import {View, Button, StyleSheet} from 'react-native';

export interface Props {}

class PickLocation extends Component<Props> {
    render() {
        return(
            <View style={styles.container}>
                <View style={styles.placeholder}>
                </View>
                <View style={styles.button}>
                    <Button 
                        title="Lokalizacja"
                        onPress={() => alert('pick image')}/>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        width: "100%",
        alignItems: "center"

    },
    placeholder: {
        borderWidth: 1,
        borderColor: 'black',
        backgroundColor: "#eee",
        width: "80%",
        height: 150
    },
    button: {
        margin: 8
    }
});

export default PickLocation;