import React, { Component } from 'react';
import {View, Image, Button, StyleSheet} from 'react-native';
import imagePlaceholder from "../../../assets/place.jpg";

export interface Props {}

class PickImage extends Component<Props> {
    render() {
        return(
            <View style={styles.container}>
                <View style={styles.placeholder}>
                    <Image 
                    source={imagePlaceholder}
                    style={styles.prevImage}/>
                </View>
                <View style={styles.button}>
                    <Button 
                        title="Wybierz zdjęcie"
                        onPress={() => alert('pick image')}/>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        width: "100%",
        alignItems: "center"

    },
    placeholder: {
        borderWidth: 1,
        borderColor: 'black',
        backgroundColor: "#eee",
        width: "80%",
        height: 150
    },
    button: {
        margin: 8
    },
    prevImage: {
        width: "100%",
        height: "100%"
    }
});

export default PickImage;