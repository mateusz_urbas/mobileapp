import React, {Component} from 'react';
import {Button, StyleSheet, View, TextInput} from 'react-native';
import DefaultInput from '../UI/DefaultInput/DefaultInput';

export interface Props {
    placeName;
    onChangeText;
}

export const PlaceInput = (props: Props) => (
    <DefaultInput 
        placeholder="Nazwa miejsca" 
        value={props.placeName}
        onChangeText={props.onChangeText}/>
)

    // styles
const styles = StyleSheet.create({

});