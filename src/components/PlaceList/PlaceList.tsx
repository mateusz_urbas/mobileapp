import React from 'react'
import { View, StyleSheet, FlatList } from 'react-native';
import ListItem from '../ListItem/ListItem';

export interface Props {
    places: any[],
    onItemSelected(i)
}

const PlaceList = (props: Props) => {
    return (
        <FlatList 
            style={styles.listContainer}
            data={props.places}
            renderItem={(info) => (
                <ListItem 
                    placeName={info.item.name}
                    placeImage={info.item.image}
                    onItemPressed={() => props.onItemSelected(info.item.key)}/>
            )}/>
    )
};

const styles = StyleSheet.create({
    listContainer: {
        width: '100%'
    }
});

export default PlaceList;