import React, { Component } from 'react';
import { View, Text, Button, StyleSheet, TextInput, ImageBackground, Dimensions } from 'react-native';
import { Navigation, NAV } from '../../navigation/routerTypes';
import DefaultInput from '../../components/UI/DefaultInput/DefaultInput';
import HeadingText from '../../components/UI/HeadingText/HeadingText';
import MainText from '../../components/UI/MainText/MainText';
import backgroundImage from '../../../assets/test1.jpg';
import ButtonWithBackground from '../../components/UI/ButtonWithBackground/ButtonWithBackground';
export interface Props {
    navigation: Navigation;
}

interface State {
    viewMode: "portrait" | "landscape"
}

class AuthScreen extends Component<Props, State> {
    constructor(props: Props) {
        super(props);
        Dimensions.addEventListener("change", this.updateStyles)

        this.state = {
            viewMode: Dimensions.get("window").height > 500 ? "portrait" : "landscape"
        }
    }

    componentWillUnmount() {
        Dimensions.removeEventListener("change", this.updateStyles)
    }

    updateStyles = (dims) => {
        this.setState({
            viewMode: dims.window.height > 500 ? "portrait" : "landscape"
        });
    }

    loginHandler = () => {
        this.props.navigation.navigate(NAV.SharePlace)
    }

    render () {
        let headingText = null;
        if(this.state.viewMode === "portrait") {
            headingText = (
                <MainText>
                    <HeadingText>
                        Zarejestruj się
                    </HeadingText>
                </MainText>
            );
        }
        return (
            <ImageBackground source={backgroundImage} style={styles.backgroundImage}>

            <View style={styles.container}>

                    {headingText}
                    <ButtonWithBackground 
                        onPress={this.loginHandler}>
                        Przełącz na logowanie
                    </ButtonWithBackground>
                    <View style={styles.inputContainer}>
                        <DefaultInput 
                            placeholder="Adres e-mail"
                            style={styles.input} />
                        <View style={this.state.viewMode === "portrait" ? 
                            styles.portraitPasswordContainer : styles.landscapePasswordContainer}>

                            <View style={this.state.viewMode === "portrait" ? 
                                styles.portraitPasswordWrapper : styles.landscapePasswordWrapper}>
                                <DefaultInput 
                                    placeholder="Hasło"
                                    style={styles.input} />
                            </View>
                            <View style={this.state.viewMode === "portrait" ? 
                                styles.portraitPasswordWrapper : styles.landscapePasswordWrapper}>
                                <DefaultInput 
                                placeholder="Potwierdź hasło"
                                style={styles.input} />
                            </View>
                        </View>

                    </View>


                    <ButtonWithBackground 
                        onPress={this.loginHandler}>
                        Potwierdź
                    </ButtonWithBackground>
            </View>
            </ImageBackground>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    backgroundImage: {
        width: "100%",
        flex: 1
    },
    inputContainer: {
        width: "80%"
    },
    input: {
        backgroundColor: '#eee',
        borderColor: '#bbb'
    },
    landscapePasswordContainer: {
        flexDirection: "row",
        justifyContent: "space-between"
    },
    portraitPasswordContainer: {
        flexDirection: "column",
        justifyContent: "flex-start"
    },

    landscapePasswordWrapper: {
        width: "45%"
    },
    portraitPasswordWrapper: {
        width: "100%"
    }
})

export default  AuthScreen