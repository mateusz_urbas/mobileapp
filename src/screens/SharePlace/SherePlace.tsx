import React from 'react';
import {StyleSheet, View, Text, TextInput, Button, ScrollView, Image} from 'react-native';
import { connect } from 'react-redux';
import { addPlace } from '../../store/actions';
import MainText from '../../components/UI/MainText/MainText';
import HeadingText from '../../components/UI/HeadingText/HeadingText';
import { PlaceInput } from '../../components/PlaceInput/PlaceInput';
import PickImage from '../../components/PickImage/PickImage';
import PickLocation from '../../components/PickLocation/PickLocation';

export interface Props {
    onAddPlace(placeName);
}

interface State {
    placeName;
}

class SharePlaceScreen extends React.Component<Props, State> {

    state = {
        placeName: ""
    }

    placeAddedHandler = () => {
        if (this.state.placeName.trim() !== "") {
            this.props.onAddPlace(this.state.placeName);
        }
    }

    placeNameChangedHandler = val => {
        this.setState({
            placeName: val
        });
    }

    render() {   
        return (
            <ScrollView>
                <View style={styles.container}>
                    <MainText>
                        <HeadingText>
                            Podziel się z nami!
                        </HeadingText>
                    </MainText>

                    <PickImage />
                    <PickLocation />

                    <PlaceInput 
                        placeName={this.state.placeName}
                        onChangeText={this.placeNameChangedHandler}/>

                    <View style={styles.button}>
                        <Button 
                            title="Udostępnij"
                            onPress={this.placeAddedHandler}/>
                    </View>
                </View>
            </ScrollView>
        );      
    }
}

const styles = StyleSheet.create({
    container: {
        padding: 20,
        flex: 1,
        alignItems: "center"
    },
    placeholder: {
        borderWidth: 1,
        borderColor: 'black',
        backgroundColor: "#eee",
        width: "80%",
        height: 150
    },
    button: {
        margin: 8
    },
    prevImage: {
        width: "100%",
        height: "100%"
    }
});

const mapDispatchToProps = dispatch => {
    return {
        onAddPlace: (placeName) => dispatch(addPlace(placeName)),
    };
};

export default connect(null, mapDispatchToProps)(SharePlaceScreen);
