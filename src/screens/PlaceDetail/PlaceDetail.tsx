import React from 'react'
import { View, StyleSheet, Modal, Image, Text, Button, TouchableOpacity, Platform } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { Navigation } from '../../navigation/routerTypes';
import { connect } from 'react-redux';
import { deletePlace } from '../../store/actions';

export interface Props {
    navigation: Navigation;
    onDeletePlace(key);
}

class PlaceDetailScreen extends React.Component<Props> {
    
    selectedPlace = this.props.navigation.getParam('selectedPlace', null);
    
    placeDeletedHandler = () => {
        this.props.onDeletePlace(this.selectedPlace.key);
        this.props.navigation.pop();
    }
    
    render() {
        return (
            <View style={styles.container}>
                <View>
                    <Image 
                        source={this.selectedPlace.image} 
                        style={styles.placeImage} />
                    <Text style={styles.placeName}>{this.selectedPlace.name}</Text>
                </View>
                <View>
                    <TouchableOpacity onPress={this.placeDeletedHandler}>
                        <View style={styles.deleteButton}>
                            <Icon 
                                size={30}
                                name = {Platform.OS === 'android' ? "md-trash" : "ios-trash" }
                                color="red" />
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
};

const styles = StyleSheet.create({
    container: {
        margin: 22
    },
    placeImage: {
        width: "100%",
        height: 200
    },
    placeName: {
        fontWeight: "bold",
        textAlign: "center",
        fontSize: 28
    },
    deleteButton: {
        alignItems: "center",
    }
});

const mapDispatchToProps = dispatch => {
    return {
        onDeletePlace: (key) => dispatch(deletePlace(key)),
    };
};

export default connect(null,mapDispatchToProps)(PlaceDetailScreen);