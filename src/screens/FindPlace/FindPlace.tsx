import React from 'react';
import {StyleSheet, View, TouchableOpacity, Text, Animated} from 'react-native';
import { connect } from 'react-redux';
import PlaceList from '../../components/PlaceList/PlaceList';
import { Navigation, NAV } from '../../navigation/routerTypes';

export interface Props {
    navigation: Navigation;
    places: any[];
}

interface State {
    placesLoaded: boolean,
    removeAnim,
    placesAnim
}

class FindPlaceScreen extends React.Component<Props, State> {


    state = {
        placesLoaded: false,
        removeAnim: new Animated.Value(1),
        placesAnim: new Animated.Value(0)
    }

    
    placesLoadedHandler = () => {
        Animated.timing(this.state.placesAnim, {
            toValue: 1,
            duration: 500,
            useNativeDriver: true
        }).start();
    }   

    placesSearchHandler = () => {
        Animated.timing(this.state.removeAnim, {
            toValue: 0,
            duration: 500,
            useNativeDriver: true
        }).start( () => {
            this.setState({
                placesLoaded:true
            });
            this.placesLoadedHandler();
        });

    }

    itemSelectedHandler = key => {
        const selectedPlace = this.props.places.find(place => {
            return place.key === key;
        });

        this.props.navigation.push(NAV.Detail, {
            selectedPlace: selectedPlace
        });
    }


    render() {
        let content = (
            <Animated.View
                style={{
                    opacity: this.state.removeAnim,
                    transform: [
                        {
                            scale: this.state.removeAnim.interpolate({
                                inputRange: [0, 1],
                                outputRange: [2, 1],
                            })
                        }
                    ]
                }}>
                <TouchableOpacity onPress={this.placesSearchHandler}>
                    <View style={styles.searchButton}>
                        <Text style={styles.searchButtonText}>
                            Znajdź miejsce
                        </Text>
                    </View>
                </TouchableOpacity>
            </Animated.View>
        );

        if (this.state.placesLoaded) {
            content = (
                <Animated.View style={{
                    opacity: this.state.placesAnim
                }}>
                <PlaceList 
                    places={this.props.places}
                    onItemSelected={this.itemSelectedHandler}/>
                </Animated.View>
                
            );
        }

        return (
            <View style={this.state.placesLoaded ? styles.container : styles.buttonContainer}>
                {content}
            </View>
        );      
    }
}

const styles = StyleSheet.create({
    container: {
        padding: 20,
    },
    buttonContainer: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    searchButton: {
        borderColor: "orange",
        borderWidth: 3,
        borderRadius: 50,
        padding: 20
    },
    searchButtonText: {
        color: "orange",
        fontWeight: "bold",
        fontSize: 26
    }

});

const mapStateToProps = state => {
    return {
        places: state.places.places,
    };
};

export default connect(mapStateToProps)(FindPlaceScreen);
