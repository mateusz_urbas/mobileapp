import React from 'react';

import {createStackNavigator, createAppContainer, createBottomTabNavigator, createSwitchNavigator, createDrawerNavigator} from 'react-navigation';
import SharePlaceScreen from '../screens/SharePlace/SherePlace';
import FindPlaceScreen from '../screens/FindPlace/FindPlace';
import AuthScreen from '../screens/Auth/Auth';
import Icon from 'react-native-vector-icons/Ionicons';
import PlaceDetail from '../screens/PlaceDetail/PlaceDetail';
import MenuDrawer from '../navigation/menuDrawer';
import { Platform } from 'react-native';

const FindPlaceStack = createStackNavigator({
    FindPlace:{
        screen: FindPlaceScreen,
        navigationOptions:({navigation})=>{
            return {
                headerTitle: 'Miejsca',
                ...mainHeaderStyle(navigation)
            }
        }
    },
    Detail: {
        screen: PlaceDetail,
        navigationOptions:({navigation})=>{
            const name = navigation.getParam('selectedPlace', null).name;
            return {
                headerTitle: name,
            }
        }
    }
}, {
    navigationOptions:({navigation}) => {
        return {
            title: 'Miejsca',
            tabBarIcon: ({ tintColor }) => (
                <Icon 
                    name = {Platform.OS === 'android' ? "md-map" : "ios-map" }
                    size={24} 
                    color={tintColor} />
            )
        };
    }
});

const SharePlaceStack = createStackNavigator({
    SharePlace:{
        screen: SharePlaceScreen,
        navigationOptions:({navigation})=>{
            return {
                headerTitle: 'Dodaj',
                ...mainHeaderStyle(navigation)
            }
        }
    }
}, {
    navigationOptions:({navigation}) => {
        return {
            title: 'Udostępnij',
            tabBarIcon: ({ tintColor }) => (
                <Icon 
                    name = {Platform.OS === 'android' ? "md-share" : "ios-share-alt" }
                    size={24} 
                    color={tintColor} />
            )
        };
    }
});


const DashboardTabNavigator  = createBottomTabNavigator({
    FindPlaceStack,
    SharePlaceStack
}, {
    navigationOptions:() => {
        return {
            header: null,
        };
    },

    tabBarOptions: {
        activeTintColor: '#28506F',
        //activeBackgroundColor: 'green',
        // // //' - Label and icon color of the inactive tab.'
        // inactiveTintColor: 'blue',
        // // //' - Background color of the inactive tab.'
        // inactiveBackgroundColor: 'yellow',
        // // //' - Style object for the tab bar.'
        style: { borderTopWidth: 1, borderColor: 'gray', paddingTop: 8 },
        // // //' - Style object for the tab label.'
         labelStyle: { fontWeight: 'bold' },
        // // //' - Style object for the tab.'
         tabStyle: { paddingBottom: 5 },
        // showLabel: false,
        // showIcon: false,
    },
});

const DashboardStackNavigator = createStackNavigator({
    DashboardTabNavigator: DashboardTabNavigator
});

const AppDrawerNavigator = createDrawerNavigator({
    DashboardStackNavigator : {
        screen: DashboardStackNavigator,
        navigationOptions:({navigation})=>{
            return {
                title: 'Strona główna',
            }
        }
    },
}, {
contentComponent:({navigation})=>{
    return ( <MenuDrawer navigation={navigation} /> );
}
});

const AuthStack = createStackNavigator({
    Auth:{
        screen: AuthScreen,
        navigationOptions:({navigation})=>{
            return {
                headerTitle: 'Autoryzacja',
            }
        }
    }
});

const AppSwitchNavigator = createSwitchNavigator({
    AuthStack,
    AppDrawerNavigator,
  });


const mainHeaderStyle = (navigation) => {
    return {
        headerStyle: {        
            backgroundColor: '#f4511e',
        },
        headerLeft:(
            <Icon 
                name="md-menu"
                size={30}
                style={{paddingLeft: 10}}
                onPress={() => navigation.openDrawer()}
            /> 
        )
    }
}


export const AppContainer = createAppContainer(AppSwitchNavigator);
