import React from 'react';
import {View, Text,StyleSheet,TouchableOpacity, Platform} from 'react-native';
import { Navigation, NAV } from './routerTypes';
import Icon from 'react-native-vector-icons/Ionicons';

export interface Props {
    navigation: Navigation;
}


export default class MenuDrawer extends React.Component<Props> {
    
    onSelectOptionHandler = (nav) => {
        this.props.navigation.closeDrawer();
        this.props.navigation.navigate(nav);
    }
    
    navLink(nav, text) {
		return(
			<TouchableOpacity style={{height: 50}} onPress={() => this.onSelectOptionHandler(nav)}>
				<Text>{text}</Text>
			</TouchableOpacity>
		)
	}

	render() {
		return(
			<View style={styles.container}>
				<TouchableOpacity onPress={()=> this.onSelectOptionHandler(NAV.Auth)}>
					<View style={styles.drawerItem}>
						<Icon 
							name = {Platform.OS === 'android' ? "md-log-out" : "ios-log-out" }
							size={30} 
							color="#aaa"
							style={styles.drawerItemIcon}/>
						<Text>Wyloguj się</Text>
					</View>
				</TouchableOpacity>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		paddingTop: 50,
		flex: 1,
		backgroundColor: 'white',
	},
	drawerItem: {
		flexDirection: "row",
		alignItems: "center",
		padding: 10,
		backgroundColor: "#eee"
	},
	drawerItemIcon: {
		marginRight: 10
	}

})