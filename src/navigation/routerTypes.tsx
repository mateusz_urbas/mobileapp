import { NavigationScreenProp, NavigationState, NavigationParams } from 'react-navigation';


export const NAV = {
    Auth: "Auth",
    SharePlace: "SharePlace",
    FindPlace: "FindPlace",
    Detail: "Detail"
}

export type Navigation = NavigationScreenProp<NavigationState,NavigationParams>;

